import { Request, Response } from "express";

import ADDRESS_CreateService from "../services/ADDRESS_CreateService";

class AddressController {
  async create(req: Request, res: Response): Promise<Response> {
    try {
      const { id } = req.user;
      const { street, number, district, city, state, zipcode, lat, long } =
        req.body;
      const addressService = new ADDRESS_CreateService();

      const address = await addressService.execute({
        user_id: Number(id),
        street,
        number,
        district,
        city,
        state,
        zipcode,
        lat,
        long,
      });

      return res.status(201).json(address);
    } catch (error) {
      return res.status(400).json({ message: error.message });
    }
  }
}
export { AddressController };
