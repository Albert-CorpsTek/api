import { Request, Response } from "express";

import SUBCATEGORY_CreateService from "../services/SUBCATEGORY_CreateService";
import SUBCATEGORY_ListPerCategoryService from "../services/SUBCATEGORY_ListPerCategoryService";
import SUBCATEGORY_ListService from "../services/SUBCATEGORY_ListService";
import SUBCATEGORY_ShowService from "../services/SUBCATEGORY_ShowService";
import SUBCATEGORY_UpdateService from "../services/SUBCATEGORY_UpdateService";

class SubcategoryController {
  async create(req: Request, res: Response): Promise<Response> {
    try {
      const { id } = req.user;
      const { category_id, name, description } = req.body;
      const subcategoryService = new SUBCATEGORY_CreateService();

      const subcategory = await subcategoryService.execute({
        user_id: Number(id),
        category_id,
        name,
        description,
      });

      return res.status(201).json(subcategory);
    } catch (error) {
      return res.status(400).json({ message: error.message });
    }
  }

  async show(req: Request, res: Response): Promise<Response> {
    try {
      const user_id = req.user.id;
      const { id } = req.params;
      const subcategoryService = new SUBCATEGORY_ShowService();

      const subcategory = await subcategoryService.execute({
        user_id: Number(user_id),
        subcategory_id: Number(id),
      });

      return res.status(201).json(subcategory);
    } catch (error) {
      return res.status(400).json({ message: error.message });
    }
  }

  async list(req: Request, res: Response): Promise<Response> {
    try {
      const { id } = req.user;

      const subcategoryService = new SUBCATEGORY_ListService();

      const subcategory = await subcategoryService.execute({
        user_id: Number(id),
      });

      return res.status(201).json(subcategory);
    } catch (error) {
      return res.status(400).json({ message: error.message });
    }
  }

  async listPerCat(req: Request, res: Response): Promise<Response> {
    try {
      const { id } = req.user;
      const { category_id } = req.params;
      console.log(category_id);
      const subcategoryService = new SUBCATEGORY_ListPerCategoryService();

      const subcategory = await subcategoryService.execute({
        user_id: Number(id),
        category_id: Number(category_id),
      });

      return res.status(201).json(subcategory);
    } catch (error) {
      return res.status(400).json({ message: error.message });
    }
  }

  async update(req: Request, res: Response): Promise<Response> {
    try {
      const { id } = req.user;
      const { subcategory_id, name, description } = req.body;
      const subcategoryService = new SUBCATEGORY_UpdateService();

      const subcategory = await subcategoryService.execute({
        user_id: Number(id),
        subcategory_id,
        name,
        description,
      });

      return res.status(201).json(subcategory);
    } catch (error) {
      return res.status(400).json({ message: error.message });
    }
  }
}
export { SubcategoryController };
