import { Request, Response } from "express";

import CATEGORY_CreateService from "../services/CATEGORY_CreateService";
import CATEGORY_ListService from "../services/CATEGORY_ListService";
import CATEGORY_ShowService from "../services/CATEGORY_ShowService";
import CATEGORY_UpdateService from "../services/CATEGORY_UpdateService";

class CategoryController {
  async create(req: Request, res: Response): Promise<Response> {
    try {
      const { id } = req.user;
      const { name, description, icon_url } = req.body;
      const categoryService = new CATEGORY_CreateService();

      const category = await categoryService.execute({
        user_id: Number(id),
        name,
        description,
        icon_url,
      });

      return res.status(201).json(category);
    } catch (error) {
      return res.status(400).json({ message: error.message });
    }
  }

  async update(req: Request, res: Response): Promise<Response> {
    try {
      const { id } = req.user;
      const { name, icon_url, category_id } = req.body;
      const categoryService = new CATEGORY_UpdateService();

      const category = await categoryService.execute({
        user_id: Number(id),
        category_id,
        name,
        icon_url,
      });

      return res.status(201).json(category);
    } catch (error) {
      return res.status(400).json({ message: error.message });
    }
  }

  async list(req: Request, res: Response): Promise<Response> {
    try {
      const { id } = req.user;
      const categoryService = new CATEGORY_ListService();

      const category = await categoryService.execute({
        user_id: Number(id),
      });

      return res.status(201).json(category);
    } catch (error) {
      return res.status(400).json({ message: error.message });
    }
  }

  async show(req: Request, res: Response): Promise<Response> {
    try {
      const user_id = req.user.id;
      const { id } = req.params;
      const categoryService = new CATEGORY_ShowService();

      const category = await categoryService.execute({
        user_id: Number(user_id),
        category_id: Number(id),
      });

      return res.status(201).json(category);
    } catch (error) {
      return res.status(400).json({ message: error.message });
    }
  }
}
export { CategoryController };
