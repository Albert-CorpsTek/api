import { Request, Response } from "express";

import ADDRESS_CreateService from "../services/ADDRESS_CreateService";
import USER_CreateScheduleService from "../services/USER_CreateScheduleService";
import USER_CreateService from "../services/USER_CreateService";
import USER_CreateSubcategoryService from "../services/USER_CreateSubcategoryService";
import USER_ListAllSubcategoryService from "../services/USER_ListAllSubcategoryService";
import USER_ListScheduleService from "../services/USER_ListScheduleService";
import USER_ListService from "../services/USER_ListService";
import USER_ListSubcategoryService from "../services/USER_ListSubcategoryService";
import USER_ShowService from "../services/USER_ShowService";
import USER_UpdateScheduleService from "../services/USER_UpdateScheduleService";
import USER_UpdateSubcategoryService from "../services/USER_UpdateSubcategoryService";

class UserController {
  async create(req: Request, res: Response): Promise<Response> {
    try {
      const {
        password,
        email,
        phone,
        name,
        plan_id,
        origin,
        lastname,
        user_type_id,
        document,
        birth,
        street,
        number,
        district,
        city,
        state,
        zipcode,
        lat,
        long,
      } = req.body;
      const usersService = new USER_CreateService();

      const user = await usersService.execute({
        user_type_id,
        password,
        document,
        birth,
        email,
        phone,
        name,
        fullname: `${name} ${lastname}`,
        plan_id,
        origin,
      });

      const addressService = new ADDRESS_CreateService();

      const address = await addressService.execute({
        user_id: user.id,
        street,
        number,
        district,
        city,
        state,
        zipcode,
        lat,
        long,
      });
      return res.status(201).json({ ...user, address });
    } catch (error) {
      return res.status(400).json({ message: error.message });
    }
  }

  async list(req: Request, res: Response): Promise<Response> {
    try {
      const usersService = new USER_ListService();
      const { filter } = req.query;

      const users = await usersService.execute({ filter: filter as string });

      return res.status(201).json(users);
    } catch (error) {
      return res.status(400).json({ message: error.message });
    }
  }

  async show(req: Request, res: Response): Promise<Response> {
    try {
      const { id } = req.params;
      const usersService = new USER_ShowService();

      const users = await usersService.execute(Number(id));

      return res.status(201).json(users);
    } catch (error) {
      return res.status(400).json({ message: error.message });
    }
  }

  async createSchedule(req: Request, res: Response): Promise<Response> {
    try {
      const { id } = req.user;
      const { weekday, start_time, finish_time } = req.body;
      const scheduleService = new USER_CreateScheduleService();

      const schedule = await scheduleService.execute({
        user_id: Number(id),
        weekday,
        start_time,
        finish_time,
      });
      console.log(schedule);
      return res.status(201).json(schedule);
    } catch (error) {
      return res.status(400).json({ message: error.message });
    }
  }

  async listSchedule(req: Request, res: Response): Promise<Response> {
    try {
      const { id } = req.user;

      const scheduleService = new USER_ListScheduleService();
      const schedule = await scheduleService.execute({
        user_id: Number(id),
      });
      return res.status(201).json(schedule);
    } catch (error) {
      return res.status(400).json({ message: error.message });
    }
  }

  async updateSchedule(req: Request, res: Response): Promise<Response> {
    try {
      const { id } = req.user;
      const { weekday_id, start_time, finish_time } = req.body;

      const scheduleService = new USER_UpdateScheduleService();
      const schedule = await scheduleService.execute({
        user_id: Number(id),
        weekday_id,
        start_time,
        finish_time,
      });
      return res.status(201).json(schedule);
    } catch (error) {
      return res.status(400).json({ message: error.message });
    }
  }

  async createSubcategories(req: Request, res: Response): Promise<Response> {
    try {
      const { id } = req.user;
      const { subcategory_id, price } = req.body;
      const subcategoriesUserService = new USER_CreateSubcategoryService();

      const subs = await subcategoriesUserService.execute({
        user_id: Number(id),
        subcategory_id,
        price,
      });
      return res.status(201).json(subs);
    } catch (error) {
      return res.status(400).json({ message: error.message });
    }
  }

  async listSubcategories(req: Request, res: Response): Promise<Response> {
    try {
      const { id } = req.user;
      const subcategoriesUserService = new USER_ListSubcategoryService();

      const subs = await subcategoriesUserService.execute({
        user_id: Number(id),
      });
      return res.status(201).json(subs);
    } catch (error) {
      return res.status(400).json({ message: error.message });
    }
  }

  async updateSubcategories(req: Request, res: Response): Promise<Response> {
    try {
      const { id } = req.user;
      const { subcategoryUser_id, price } = req.body;
      const subcategoriesUserService = new USER_UpdateSubcategoryService();

      const subs = await subcategoriesUserService.execute({
        user_id: Number(id),
        subcategoryUser_id,
        price,
      });
      return res.status(201).json(subs);
    } catch (error) {
      return res.status(400).json({ message: error.message });
    }
  }

  async listallUserSubcategories(
    req: Request,
    res: Response
  ): Promise<Response> {
    try {
      const { id } = req.user;
      const subcategoriesUserService = new USER_ListAllSubcategoryService();

      const subs = await subcategoriesUserService.execute({
        user_id: Number(id),
      });
      return res.status(201).json(subs);
    } catch (error) {
      return res.status(400).json({ message: error.message });
    }
  }
}
export { UserController };
