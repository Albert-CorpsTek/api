import { Request, Response } from "express";

import OS_CreateService from "../services/OS_CreateService";

class OrderServiceController {
  async create(req: Request, res: Response): Promise<Response> {
    try {
      const { id } = req.user;

      const { data } = req.body;
      const osService = new OS_CreateService();

      const address = await osService.execute({
        user_id: Number(id),
        data,
      });

      return res.status(201).json(address);
    } catch (error) {
      return res.status(400).json({ message: error.message });
    }
  }
}
export { OrderServiceController };
