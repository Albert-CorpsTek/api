import { Request, Response } from "express";

import USER_AuthenticateService from "../services/USER_AuthenticateService";

export default class SessionsController {
  public async create(req: Request, res: Response): Promise<Response> {
    try {
      const { document, password } = req.body;

      const authenticateUser = new USER_AuthenticateService();

      const { user, token } = await authenticateUser.execute({
        document,
        password,
      });

      return res.json({ user, token });
    } catch (error) {
      return res.status(400).json({ message: error.message });
    }
  }
}
