import "reflect-metadata";
import "dotenv/config";
import cors from "cors";
import express from "express";
import "./database";

import uploadConfig from "./config/upload";
import routes from "./routes";

const app = express();

app.use(cors());
app.use(express.json({ limit: "50mb" }));
// app.use(express.urlencoded({ limit: "50mb" }));
app.use("/files", express.static(uploadConfig.directory));
app.use(routes);

app.listen(3333, () => console.log("Robocomp only in port 3333"));
