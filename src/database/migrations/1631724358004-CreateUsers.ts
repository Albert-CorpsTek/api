import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableColumn,
  TableForeignKey,
} from "typeorm";

export default class CreateUsers1631724358004 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: "users",
        columns: [
          {
            name: "id",
            type: "integer",
            length: "11",
            isPrimary: true,
            isGenerated: true,
            generationStrategy: "increment",
          },
          {
            name: "plan_id",
            type: "integer",
            length: "11",
            isNullable: true,
          },
          {
            name: "user_type_id",
            type: "integer",
            length: "11",
          },
          {
            name: "password",
            type: "varchar",
          },
          {
            name: "name",
            type: "varchar",
          },
          {
            name: "fullname",
            type: "varchar",
          },
          {
            name: "email",
            type: "varchar",
            isUnique: true,
          },
          {
            name: "document",
            type: "varchar",
            isNullable: true,
          },
          {
            name: "photo",
            type: "varchar",
            isNullable: true,
          },
          {
            name: "birth",
            type: "date",
            isNullable: true,
          },
          {
            name: "total_rating",
            type: "integer",
            isNullable: true,
          },
          {
            name: "rating",
            type: "decimal(3,1)",
            isNullable: true,
          },
          {
            name: "active",
            type: "tinyint",
          },
          {
            name: "isAdmin",
            type: "tinyint",
          },
          {
            name: "lead",
            type: "tinyint",
          },
          {
            name: "phone", // usuario só pode vender se pagar
            type: "varchar",
            isNullable: true,
          },
          {
            name: "origin",
            type: "enum('mobile','web')",
          },
          {
            name: "created_at",
            type: "timestamp",
            default: "now()",
          },
          {
            name: "updated_at",
            type: "timestamp",
            default: "now()",
          },
        ],
        foreignKeys: [
          {
            name: "FKUsers_Plans",
            referencedTableName: "plans",
            referencedColumnNames: ["id"],
            columnNames: ["plan_id"],
            onDelete: "CASCADE",
          },
          {
            name: "FKUsers_UserTypes",
            referencedTableName: "user_types",
            referencedColumnNames: ["id"],
            columnNames: ["user_type_id"],
            onDelete: "CASCADE",
          },
        ],
      })
    );

    await queryRunner.addColumn(
      "users",
      new TableColumn({
        name: "user_up_id",
        type: "int",
        isNullable: true,
      })
    );

    await queryRunner.createForeignKey(
      "users",
      new TableForeignKey({
        columnNames: ["user_up_id"],
        referencedColumnNames: ["id"],
        referencedTableName: "users",
        onDelete: "CASCADE",
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    const table = await queryRunner.getTable("users");
    const foreignKey = table.foreignKeys.find(
      (fk) => fk.columnNames.indexOf("user_up_id") !== -1
    );
    await queryRunner.dropForeignKey("users", foreignKey);
    await queryRunner.dropColumn("users", "user_up_id");
    await queryRunner.dropTable("users");
  }
}
