import { MigrationInterface, QueryRunner, Table } from "typeorm";

export default class CreateSubcategoriesUsers1638907561060
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: "subcategories_users",
        columns: [
          {
            name: "id",
            type: "integer",
            length: "11",
            isPrimary: true,
            isGenerated: true,
            generationStrategy: "increment",
          },
          {
            name: "user_id",
            type: "integer",
            isNullable: false,
          },
          {
            name: "subcategory_id",
            type: "integer",
            isNullable: false,
          },
          {
            name: "price",
            type: "decimal(10,2)",
          },
          {
            name: "created_at",
            type: "timestamp",
            default: "now()",
          },
          {
            name: "updated_at",
            type: "timestamp",
            default: "now()",
          },
        ],
        foreignKeys: [
          {
            name: "FKSubcategory_User",
            referencedTableName: "subcategories",
            referencedColumnNames: ["id"],
            columnNames: ["subcategory_id"],
            onDelete: "CASCADE",
          },
          {
            name: "FKUser_Subcategory",
            referencedTableName: "users",
            referencedColumnNames: ["id"],
            columnNames: ["user_id"],
            onDelete: "CASCADE",
          },
        ],
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable("subcategories_users");
  }
}
