import { MigrationInterface, QueryRunner, Table } from "typeorm";

export default class CreateOrderServiccTec1638910528451
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: "order_services_users",
        columns: [
          {
            name: "id",
            type: "integer",
            length: "11",
            isPrimary: true,
            isGenerated: true,
            generationStrategy: "increment",
          },
          {
            name: "user_id",
            type: "integer",
            isNullable: false,
          },
          {
            name: "os_id",
            type: "integer",
            isNullable: false,
          },
          {
            name: "status",
            type: "enum('pendente','aceita','negada','em andamento', 'finalizada', 'avaliada')",
          },
          {
            name: "created_at",
            type: "timestamp",
            default: "now()",
          },
          {
            name: "updated_at",
            type: "timestamp",
            default: "now()",
          },
        ],
        foreignKeys: [
          {
            name: "FKOSTec_OS",
            referencedTableName: "users",
            referencedColumnNames: ["id"],
            columnNames: ["user_id"],
            onDelete: "CASCADE",
          },
          {
            name: "FKOS_OSTec",
            referencedTableName: "order_services",
            referencedColumnNames: ["id"],
            columnNames: ["os_id"],
            onDelete: "CASCADE",
          },
        ],
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable("order_services_users");
  }
}
