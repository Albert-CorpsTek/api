import { MigrationInterface, QueryRunner, Table } from "typeorm";

export class CreateOrderServices1638908259180 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: "order_services",
        columns: [
          {
            name: "id",
            type: "integer",
            length: "11",
            isPrimary: true,
            isGenerated: true,
            generationStrategy: "increment",
          },
          {
            name: "user_id",
            type: "integer",
            isNullable: false,
          },
          {
            name: "tec_id",
            type: "integer",
            isNullable: false,
          },
          {
            name: "subcat_tec_id",
            type: "integer",
            isNullable: false,
          },
          {
            name: "address_id",
            type: "integer",
            isNullable: false,
          },
          {
            name: "note",
            type: "varchar",
          },
          {
            name: "value",
            type: "decimal(10,2)",
          },
          {
            name: "status",
            type: "enum('pendente','aceita','negada','em andamento', 'finalizada', 'avaliada')",
          },
          {
            name: "created_at",
            type: "timestamp",
            default: "now()",
          },
          {
            name: "updated_at",
            type: "timestamp",
            default: "now()",
          },
        ],
        foreignKeys: [
          {
            name: "FKOS_User",
            referencedTableName: "users",
            referencedColumnNames: ["id"],
            columnNames: ["user_id"],
            onDelete: "CASCADE",
          },
          {
            name: "FKOS_Tec",
            referencedTableName: "users",
            referencedColumnNames: ["id"],
            columnNames: ["tec_id"],
            onDelete: "CASCADE",
          },
          {
            name: "FKOS_SubcatTec",
            referencedTableName: "subcategories_users",
            referencedColumnNames: ["id"],
            columnNames: ["subcat_tec_id"],
            onDelete: "CASCADE",
          },
          {
            name: "FKOS_Address",
            referencedTableName: "addresses",
            referencedColumnNames: ["id"],
            columnNames: ["address_id"],
            onDelete: "CASCADE",
          },
        ],
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable("order_services");
  }
}
