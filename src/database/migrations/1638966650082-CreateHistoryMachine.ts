import { MigrationInterface, QueryRunner, Table } from "typeorm";

export default class CreateHistoryMachine1638966650082
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: "history_machines",
        columns: [
          {
            name: "id",
            type: "integer",
            length: "11",
            isPrimary: true,
            isGenerated: true,
            generationStrategy: "increment",
          },
          {
            name: "user_id",
            type: "integer",
            isNullable: false,
          },
          {
            name: "tec_id",
            type: "integer",
            isNullable: false,
          },
          {
            name: "os_id",
            type: "integer",
            isNullable: false,
          },
          {
            name: "serial_code",
            type: "varchar",
          },
          {
            name: "problem",
            type: "varchar",
          },
          {
            name: "description",
            type: "varchar",
          },
          {
            name: "suggestion",
            type: "varchar",
          },
          {
            name: "created_at",
            type: "timestamp",
            default: "now()",
          },
          {
            name: "updated_at",
            type: "timestamp",
            default: "now()",
          },
        ],
        foreignKeys: [
          {
            name: "FKHistory_User",
            referencedTableName: "users",
            referencedColumnNames: ["id"],
            columnNames: ["user_id"],
            onDelete: "CASCADE",
          },
          {
            name: "FKHistory_Tec",
            referencedTableName: "users",
            referencedColumnNames: ["id"],
            columnNames: ["tec_id"],
            onDelete: "CASCADE",
          },
          {
            name: "FKHistory_OS",
            referencedTableName: "order_services",
            referencedColumnNames: ["id"],
            columnNames: ["os_id"],
            onDelete: "CASCADE",
          },
        ],
      })
    );
  }
  d;
  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable("history_machines");
  }
}
