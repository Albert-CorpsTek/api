import { MigrationInterface, QueryRunner, Table } from "typeorm";

export default class CreateAddress1638902928026 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: "addresses",
        columns: [
          {
            name: "id",
            type: "integer",
            length: "11",
            isPrimary: true,
            isGenerated: true,
            generationStrategy: "increment",
          },
          {
            name: "user_id",
            type: "integer",
            isNullable: false,
          },
          {
            name: "user_up_id",
            type: "integer",
            isNullable: true,
          },
          {
            name: "street",
            type: "varchar",
          },
          {
            name: "district",
            type: "varchar",
          },
          {
            name: "number",
            type: "varchar",
          },
          {
            name: "city",
            type: "varchar",
          },
          {
            name: "state",
            type: "varchar",
          },
          {
            name: "zipcode",
            type: "varchar",
          },
          {
            name: "lat",
            type: "varchar",
          },
          {
            name: "long",
            type: "varchar",
          },
          {
            name: "active",
            type: "tinyint",
          },
          {
            name: "created_at",
            type: "timestamp",
            default: "now()",
          },
          {
            name: "updated_at",
            type: "timestamp",
            default: "now()",
          },
        ],
        foreignKeys: [
          {
            name: "FKUsers_Address",
            referencedTableName: "users",
            referencedColumnNames: ["id"],
            columnNames: ["user_id"],
            onDelete: "CASCADE",
          },
          {
            name: "FKUsers_AddressUpload",
            referencedTableName: "users",
            referencedColumnNames: ["id"],
            columnNames: ["user_up_id"],
            onDelete: "CASCADE",
          },
        ],
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable("addresses");
  }
}
