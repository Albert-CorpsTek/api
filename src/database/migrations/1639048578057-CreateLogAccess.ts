import { MigrationInterface, QueryRunner, Table } from "typeorm";

export default class CreateLogAccess1639048578057
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: "log_access",
        columns: [
          {
            name: "id",
            type: "integer",
            length: "11",
            isPrimary: true,
            isGenerated: true,
            generationStrategy: "increment",
          },
          {
            name: "user_id",
            type: "integer",
            length: "11",
          },
          {
            name: "lat",
            type: "varchar",
          },
          {
            name: "long",
            type: "varchar",
          },
          {
            name: "ip",
            type: "varchar",
          },
          {
            name: "user_agent",
            type: "varchar",
          },
          {
            name: "url",
            type: "varchar",
          },
          {
            name: "method",
            type: "varchar",
          },
          {
            name: "created_at",
            type: "timestamp",
            default: "now()",
          },
        ],
        foreignKeys: [
          {
            name: "FKLogAccess_Users",
            referencedTableName: "users",
            referencedColumnNames: ["id"],
            columnNames: ["user_id"],
            onDelete: "CASCADE",
          },
        ],
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable("log_access");
  }
}
