import { MigrationInterface, QueryRunner, Table } from "typeorm";

export default class CreatePlans1631715606732 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: "plans",
        columns: [
          {
            name: "id",
            type: "integer",
            length: "11",
            isPrimary: true,
            isGenerated: true,
            generationStrategy: "increment",
          },
          {
            name: "description",
            type: "varchar",
          },
          {
            name: "value",
            type: "decimal(9,2)",
          },
          {
            name: "radius",
            type: "int",
          },
          {
            name: "title",
            type: "varchar",
          },
          {
            name: "is_admin",
            type: "tinyint",
          },
          {
            name: "is_mobile",
            type: "tinyint",
          },
          {
            name: "priority",
            type: "int",
          },
          {
            name: "priority_time",
            type: "time",
          },
          {
            name: "created_at",
            type: "timestamp",
            default: "now()",
          },
          {
            name: "updated_at",
            type: "timestamp",
            default: "now()",
          },
        ],
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable("plans");
  }
}
