import { Router } from "express";

import { OrderServiceController } from "../controllers/OrderServiceController";
import ensureAuthenticate from "../middlewares/ensureAuthenticated";

const osRoutes = Router();
const orderServiceController = new OrderServiceController();

osRoutes.post("/", ensureAuthenticate, orderServiceController.create);

export { osRoutes };
