import { Router } from "express";

import { addressRoutes } from "./address.routes";
import { categoryRoutes } from "./category.routes";
import { osRoutes } from "./os.routes";
import { sessionsRoutes } from "./session.routes";
import { subcategoryRoutes } from "./subcategory.routes";
import { userRoutes } from "./user.routes";

const routes = Router();

routes.use("/address", addressRoutes);
routes.use("/category", categoryRoutes);
routes.use("/subcategory", subcategoryRoutes);
routes.use("/user", userRoutes);
routes.use("/os", osRoutes);
routes.use("/session", sessionsRoutes);

export default routes;
