import { Router } from "express";

import { AddressController } from "../controllers/AddressController";
// import ensureAuthenticate from "../middlewares/ensureAuthenticated";

const addressRoutes = Router();
const addressController = new AddressController();

addressRoutes.post("/", addressController.create);

export { addressRoutes };
