import { Router } from "express";

import { UserController } from "../controllers/UserController";
import ensureAuthenticate from "../middlewares/ensureAuthenticated";

const userRoutes = Router();
const userController = new UserController();

userRoutes.post("/schedule", ensureAuthenticate, userController.createSchedule);
userRoutes.get("/schedule", ensureAuthenticate, userController.listSchedule);
userRoutes.put("/schedule", ensureAuthenticate, userController.updateSchedule);

userRoutes.get(
  "/alltec",
  ensureAuthenticate,
  userController.listallUserSubcategories
);

userRoutes.post(
  "/subcategory",
  ensureAuthenticate,
  userController.createSubcategories
);
userRoutes.get(
  "/subcategory",
  ensureAuthenticate,
  userController.listSubcategories
);

userRoutes.put(
  "/subcategory",
  ensureAuthenticate,
  userController.updateSubcategories
);

userRoutes.post("/", userController.create);
userRoutes.get("/", ensureAuthenticate, userController.list);
userRoutes.get("/:id", ensureAuthenticate, userController.show);

export { userRoutes };
