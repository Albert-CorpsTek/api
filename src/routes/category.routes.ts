import { Router } from "express";

import { CategoryController } from "../controllers/CategoryController";
import ensureAuthenticate from "../middlewares/ensureAuthenticated";

const categoryRoutes = Router();
const categoryController = new CategoryController();

categoryRoutes.post("/", ensureAuthenticate, categoryController.create);
categoryRoutes.get("/", ensureAuthenticate, categoryController.list);
categoryRoutes.get("/:id", ensureAuthenticate, categoryController.show);
categoryRoutes.put("/", ensureAuthenticate, categoryController.update);

export { categoryRoutes };
