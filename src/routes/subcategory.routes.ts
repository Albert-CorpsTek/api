import { Router } from "express";

import { SubcategoryController } from "../controllers/SubcategoryController";
import ensureAuthenticate from "../middlewares/ensureAuthenticated";

const subcategoryRoutes = Router();
const subcategoryController = new SubcategoryController();

subcategoryRoutes.get(
  "/category/:category_id",
  ensureAuthenticate,
  subcategoryController.listPerCat
);

subcategoryRoutes.post("/", ensureAuthenticate, subcategoryController.create);
subcategoryRoutes.get("/", ensureAuthenticate, subcategoryController.list);
subcategoryRoutes.get("/:id", ensureAuthenticate, subcategoryController.show);
subcategoryRoutes.put("/", ensureAuthenticate, subcategoryController.update);

export { subcategoryRoutes };
