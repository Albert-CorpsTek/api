import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

import { OrderService } from "./OrderService";
import { User } from "./User";

@Entity("order_services_users")
class OrderServiceUser {
  @PrimaryGeneratedColumn()
  id: number;

  @JoinColumn({ name: "os_id" })
  @ManyToOne(() => OrderService)
  os: OrderService;

  @Column()
  os_id: number;

  @JoinColumn({ name: "user_id" })
  @ManyToOne(() => User)
  user: User;

  @Column()
  user_id: number;

  @Column()
  status:
    | "requisitada"
    | "aceita"
    | "negada"
    | "em andamento"
    | "finalizada"
    | "avaliada";

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}

export { OrderServiceUser };
