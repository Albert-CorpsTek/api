import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

import { Category } from "./Category";
import { User } from "./User";

@Entity("subcategories")
class Subcategory {
  @PrimaryGeneratedColumn()
  id: number;

  @JoinColumn({ name: "user_id" })
  @ManyToOne(() => User)
  user: User;

  @Column()
  user_id: number;

  @JoinColumn({ name: "category_id" })
  @ManyToOne(() => Category)
  category: Category;

  @Column()
  category_id: number;

  @Column()
  description: string;

  @Column()
  name: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}

export { Subcategory };
