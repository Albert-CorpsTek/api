import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

import { OrderService } from "./OrderService";

@Entity("images")
class Image {
  @PrimaryGeneratedColumn()
  id: number;

  @JoinColumn({ name: "os_id" })
  @ManyToOne(() => OrderService)
  os: OrderService;

  @Column()
  os_id: number;

  @Column()
  url: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}

export { Image };
