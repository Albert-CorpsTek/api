import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from "typeorm";

@Entity("plans")
class Plan {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  description: string;

  @Column()
  value: number;

  @Column()
  radius: number;

  @Column()
  title: string;

  @Column()
  is_admin: number;

  @Column()
  is_mobile: number;

  @Column()
  priority: number;

  @Column()
  priority_time: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}

export { Plan };
