import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

import { OrderService } from "./OrderService";
import { User } from "./User";

@Entity("history_machines")
class HistoryMachine {
  @PrimaryGeneratedColumn()
  id: number;

  @JoinColumn({ name: "user_id" })
  @ManyToOne(() => User)
  user: User;

  @Column()
  user_id: number;

  @JoinColumn({ name: "tec_id" })
  @ManyToOne(() => User)
  tec: User;

  @Column()
  tec_id: number;

  @JoinColumn({ name: "os_id" })
  @ManyToOne(() => OrderService)
  os: OrderService;

  @Column()
  os_id: number;

  @Column()
  serial_code: string;

  @Column()
  problem: string;

  @Column()
  description: string;

  @Column()
  suggestion: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}

export { HistoryMachine };
