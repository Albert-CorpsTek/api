import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

import { Address } from "./Address";
import { Plan } from "./Plan";
import { SubcategoriesUser } from "./SubcategoriesUser";
import { UserType } from "./UserType";

@Entity("users")
class User {
  @PrimaryGeneratedColumn()
  id: number;

  @OneToOne(() => Address, (address) => address.user)
  address?: Address;

  @OneToMany(() => SubcategoriesUser, (subcategories) => subcategories.user)
  subcategories?: SubcategoriesUser;

  @Column()
  plan_id: number;

  @ManyToOne(() => Plan)
  @JoinColumn({ name: "plan_id" })
  plan: Plan;

  @Column()
  user_type_id: number;

  @ManyToOne(() => UserType)
  @JoinColumn({ name: "user_type_id" })
  user_type: UserType;

  @Column()
  active: number;

  @Column()
  lead: number;

  @Column()
  name: string;

  @Column()
  fullname: string;

  @Column()
  password: string;

  @Column()
  email: string;

  @Column()
  document: string;

  @Column()
  photo?: string;

  @Column()
  phone?: string;

  @Column()
  total_rating?: string;

  @Column()
  rating?: string;

  @Column()
  isAdmin?: number;

  @Column()
  birth: Date;

  @Column()
  origin: "mobile" | "web";

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @Column()
  user_up_id?: string;

  @JoinColumn({ name: "user_up_id" })
  @ManyToOne(() => User)
  user_update: User;
}

export { User };
