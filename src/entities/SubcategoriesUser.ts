import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

import { Subcategory } from "./Subcategory";
import { User } from "./User";

@Entity("subcategories_users")
class SubcategoriesUser {
  @PrimaryGeneratedColumn()
  id: number;

  @JoinColumn({ name: "subcategory_id" })
  @ManyToOne(() => Subcategory)
  subcategory: Subcategory;

  @Column()
  subcategory_id: number;

  @JoinColumn({ name: "user_id" })
  @ManyToOne(() => User)
  user: User;

  @Column()
  user_id: number;

  @Column()
  price: number;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}

export { SubcategoriesUser };
