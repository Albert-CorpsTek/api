import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

import { User } from "./User";

@Entity("addresses")
class Address {
  @PrimaryGeneratedColumn()
  id: number;

  @JoinColumn({ name: "user_id" })
  @OneToOne(() => User, (user) => user.id)
  user: User;

  @Column()
  user_id: number;

  @JoinColumn({ name: "user_up_id" })
  @ManyToOne(() => User)
  user_update: User;

  @Column()
  user_up_id?: number;

  @Column()
  street: string;

  @Column()
  number: string;

  @Column()
  district: string;

  @Column()
  city: string;

  @Column()
  state: string;

  @Column()
  zipcode: string;

  @Column()
  lat: string;

  @Column()
  long: string;

  @Column()
  active: number;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}

export { Address };
