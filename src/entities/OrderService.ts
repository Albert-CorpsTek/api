import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

import { Address } from "./Address";
import { Subcategory } from "./Subcategory";
import { User } from "./User";

@Entity("order_services")
class OrderService {
  @PrimaryGeneratedColumn()
  id: number;

  @JoinColumn({ name: "address_id" })
  @ManyToOne(() => Address)
  address: Address;

  @Column()
  address_id: number;

  @JoinColumn({ name: "user_id" })
  @ManyToOne(() => User)
  user: User;

  @Column()
  user_id: number;

  @JoinColumn({ name: "tec_id" })
  @ManyToOne(() => User)
  tec: User;

  @Column()
  tec_id: number;

  @JoinColumn({ name: "subcat_tec_id" })
  @ManyToOne(() => Subcategory)
  subcategory: Subcategory;

  @Column()
  subcat_tec_id: number;

  @Column()
  note: string;

  @Column()
  value: number;

  @Column()
  status:
    | "requisitada"
    | "aceita"
    | "negada"
    | "em andamento"
    | "finalizada"
    | "avaliada";

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}

export { OrderService };
