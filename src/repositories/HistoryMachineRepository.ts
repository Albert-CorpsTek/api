import { EntityRepository, Repository } from "typeorm";

import { HistoryMachine } from "../entities/HistoryMachine";

@EntityRepository(HistoryMachine)
class HistoryMachineRepository extends Repository<HistoryMachine> {}

export { HistoryMachineRepository };
