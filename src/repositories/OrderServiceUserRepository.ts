import { EntityRepository, Repository } from "typeorm";

import { OrderServiceUser } from "../entities/OrderServiceUser";

@EntityRepository(OrderServiceUser)
class OrderServiceUserRepository extends Repository<OrderServiceUser> {}

export { OrderServiceUserRepository };
