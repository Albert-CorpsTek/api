import { EntityRepository, Repository } from "typeorm";

import { Plan } from "../entities/Plan";

@EntityRepository(Plan)
class PlanRepository extends Repository<Plan> {}

export { PlanRepository };
