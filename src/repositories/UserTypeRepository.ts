import { EntityRepository, Repository } from "typeorm";

import { UserType } from "../entities/UserType";

@EntityRepository(UserType)
class UserTypeRepository extends Repository<UserType> {}

export { UserTypeRepository };
