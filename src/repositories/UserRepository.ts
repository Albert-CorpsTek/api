import { EntityRepository, Repository } from "typeorm";

import { User } from "../entities/User";

@EntityRepository(User)
class UserRepository extends Repository<User> {
  private user: User;
}

export { UserRepository };
