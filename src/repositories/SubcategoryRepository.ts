import { EntityRepository, Repository } from "typeorm";

import { Subcategory } from "../entities/Subcategory";

@EntityRepository(Subcategory)
class SubcategoryRepository extends Repository<Subcategory> {}

export { SubcategoryRepository };
