import { EntityRepository, Repository } from "typeorm";

import { OrderService } from "../entities/OrderService";

@EntityRepository(OrderService)
class OrderServiceRepository extends Repository<OrderService> {}

export { OrderServiceRepository };
