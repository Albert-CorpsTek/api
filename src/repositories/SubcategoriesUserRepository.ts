import { EntityRepository, Repository } from "typeorm";

import { SubcategoriesUser } from "../entities/SubcategoriesUser";

@EntityRepository(SubcategoriesUser)
class SubcategoriesUserRepository extends Repository<SubcategoriesUser> {}

export { SubcategoriesUserRepository };
