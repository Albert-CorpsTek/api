import { compare } from "bcryptjs";
import { sign } from "jsonwebtoken";
import { Repository, getCustomRepository } from "typeorm";

import authConfig from "../config/auth";
import { Address } from "../entities/Address";
// import { Address } from "../entities/Address";
// import { SubcategoriesUser } from "../entities/SubcategoriesUser";
import { User } from "../entities/User";
// import { AddressRepository } from "../repositories/AddressRepository";
// import { SubcategoriesUSerRepository } from "../repositories/SubcategoriesUSerRepository";
import { UserRepository } from "../repositories/UserRepository";

interface IRequest {
  document: string;
  password: string;
}

interface IUser {
  user_type_id: number;
  name: string;
  fullname: string;
  email: string;
  photo?: string;
  plan_id?: number;
  origin: "mobile" | "web";
  address?: Address;
}

interface IResponse {
  user: IUser;
  token: string;
}

class USER_AuthenticateService {
  private usersRepository: Repository<User>;
  // private userAddressRepository: Repository<Address>;

  constructor() {
    this.usersRepository = getCustomRepository(UserRepository);
    // this.userAddressRepository = getCustomRepository(AddressRepository);
  }

  public async execute({ document, password }: IRequest): Promise<IResponse> {
    const userF = await this.usersRepository.findOne({
      where: { document },
      relations: ["address"],
    });

    if (!userF) {
      throw new Error("Incorrect email/password combination");
    }

    const passwordMatched = await compare(password, userF.password);

    if (!passwordMatched) {
      throw new Error("Incorrect email/password combination");
    }

    if (userF.active === 0) {
      throw new Error("Inactive account");
    }

    // let attachments = false;

    // if (userF.user_type_id !== 5) {
    //   const attach = await this.userAttachmentsRepository.find({
    //     where: {
    //       user_id: userF.id,
    //     },
    //   });

    //   if (attach.length > 0) {
    //     attachments = true;
    //   }
    // }

    const { secret, expiresIn } = authConfig.jwt;

    const token = sign({ name: userF.name }, secret, {
      subject: `${userF.id}`,
      expiresIn,
    });

    // const addresses = await this.userAddressRepository.find({
    //   where: { user_id: userF.id, active: 1 },
    // });

    // const pricesSub = await this.userSubCategoryRepository.find({
    //   where: { user_id: userF.id },
    // });

    // const prices = pricesSub.length > 0;

    delete userF.password;

    const user = {
      ...userF,
    };

    return { user, token };
  }
}

export default USER_AuthenticateService;
