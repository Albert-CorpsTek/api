import { Repository, getCustomRepository } from "typeorm";

import { Category } from "../entities/Category";
import { User } from "../entities/User";
import { CategoryRepository } from "../repositories/CategoryRepository";
import { UserRepository } from "../repositories/UserRepository";

interface IRequest {
  user_id: number;
  name: string;
  description: string;
  icon_url: string;
}

class CATEGORY_CreateService {
  private usersRepository: Repository<User>;
  private categoryRepository: Repository<Category>;

  constructor() {
    this.usersRepository = getCustomRepository(UserRepository);
    this.categoryRepository = getCustomRepository(CategoryRepository);
  }

  public async execute({
    user_id,
    name,
    icon_url,
  }: IRequest): Promise<Category> {
    if (!user_id || !name || !icon_url) {
      throw new Error("Incomplete data");
    }

    const user = await this.usersRepository.findOne({
      where: { id: user_id, isAdmin: 1 },
    });

    if (!user) {
      throw new Error("User is not exists/is not admin");
    }

    const category = this.categoryRepository.create({
      user_id,
      name,
      icon_url,
    });

    await this.categoryRepository.save(category);

    return category;
  }
}

export default CATEGORY_CreateService;
