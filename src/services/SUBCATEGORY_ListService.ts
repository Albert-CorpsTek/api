import { Repository, getCustomRepository } from "typeorm";

import { Subcategory } from "../entities/Subcategory";
import { User } from "../entities/User";
import { SubcategoryRepository } from "../repositories/SubcategoryRepository";
import { UserRepository } from "../repositories/UserRepository";

interface IRequest {
  user_id: number;
}

class SUBCATEGORY_ListService {
  private usersRepository: Repository<User>;
  private SubcategoryRepository: Repository<Subcategory>;

  constructor() {
    this.usersRepository = getCustomRepository(UserRepository);
    this.SubcategoryRepository = getCustomRepository(SubcategoryRepository);
  }

  public async execute({ user_id }: IRequest): Promise<Subcategory[]> {
    if (!user_id) {
      throw new Error("Incomplete data");
    }

    const user = await this.usersRepository.findOne({
      where: { id: user_id },
    });

    if (!user) {
      throw new Error("User is not exists");
    }

    const subcategories = await this.SubcategoryRepository.find();

    return subcategories;
  }
}

export default SUBCATEGORY_ListService;
