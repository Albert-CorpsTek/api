import { Repository, getCustomRepository } from "typeorm";

import { User } from "../entities/User";
import { UserRepository } from "../repositories/UserRepository";

interface IRequest {
  filter?: string;
}

class USER_ListService {
  private usersRepository: Repository<User>;

  constructor() {
    this.usersRepository = getCustomRepository(UserRepository);
  }

  public async execute({ filter }: IRequest): Promise<User[]> {
    if (filter === "1") {
      const users = await this.usersRepository.find({
        where: {
          user_type_id: Number(filter),
        },
        relations: ["address"],
      });
      return users;
    }
    if (filter === "2") {
      const users = await this.usersRepository.find({
        where: {
          user_type_id: Number(filter),
        },
        relations: ["address"],
      });
      return users;
    }
    if (filter === "3") {
      const users = await this.usersRepository.find({
        where: {
          user_type_id: Number(filter),
        },
        relations: [
          "address",
          "subcategories",
          "subcategories.subcategory",
          "subcategories.subcategory.category",
        ],
      });

      return users;
    }
    const users = await this.usersRepository.find({
      relations: [
        "address",
        "subcategories",
        "subcategories.subcategory",
        "subcategories.subcategory.category",
      ],
    });
    return users;
  }
}

export default USER_ListService;
