import { Repository, getCustomRepository } from "typeorm";

import { Schedule } from "../entities/Schedule";
import { ScheduleRepository } from "../repositories/ScheduleRepository";

interface IRquest {
  user_id: number;
}

class USER_ListScheduleService {
  private scheduleRepository: Repository<Schedule>;

  constructor() {
    this.scheduleRepository = getCustomRepository(ScheduleRepository);
  }

  public async execute({ user_id }: IRquest): Promise<Schedule[]> {
    const weekdays = await this.scheduleRepository.find({
      where: { user_id, active: 1 },
    });
    return weekdays;
  }
}

export default USER_ListScheduleService;
