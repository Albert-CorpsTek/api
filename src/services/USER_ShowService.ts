import { Repository, getCustomRepository } from "typeorm";

import { User } from "../entities/User";
import { UserRepository } from "../repositories/UserRepository";

class USER_ShowService {
  private usersRepository: Repository<User>;

  constructor() {
    this.usersRepository = getCustomRepository(UserRepository);
  }

  public async execute(user_id: number): Promise<User> {
    const user = await this.usersRepository.findOne({
      where: { id: user_id },
      relations: ["address"],
    });
    return user;
  }
}

export default USER_ShowService;
