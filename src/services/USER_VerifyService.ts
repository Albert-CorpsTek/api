import { Repository, getCustomRepository } from "typeorm";

import { User } from "../entities/User";
import { UserRepository } from "../repositories/UserRepository";

interface IRquest {
  email: string;
  phone: string;
}

class USER_VerifyService {
  private usersRepository: Repository<User>;

  constructor() {
    this.usersRepository = getCustomRepository(UserRepository);
  }

  public async execute({ email, phone }: IRquest): Promise<User> {
    if (!email || !phone) {
      throw new Error("Incomplete data");
    }

    const userExists = await this.usersRepository.findOne({
      where: {
        email,
        phone,
      },
    });

    if (!userExists) {
      throw new Error("User not found");
    }

    return userExists;
  }
}

export default USER_VerifyService;
