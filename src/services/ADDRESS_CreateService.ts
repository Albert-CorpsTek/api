import { Repository, getCustomRepository } from "typeorm";

import { Address } from "../entities/Address";
import { AddressRepository } from "../repositories/AddressRepository";

interface IRquest {
  user_id: number;
  street: string;
  number: string;
  district: string;
  city: string;
  state: string;
  zipcode: string;
  lat: string;
  long: string;
}

class ADDRESS_CreateService {
  private addressesRepository: Repository<Address>;

  constructor() {
    this.addressesRepository = getCustomRepository(AddressRepository);
  }

  public async execute({
    user_id,
    street,
    number,
    district,
    city,
    state,
    zipcode,
    lat,
    long,
  }: IRquest): Promise<Address> {
    if (
      !user_id ||
      !street ||
      !number ||
      !district ||
      !city ||
      !state ||
      !zipcode ||
      !lat ||
      !long
    ) {
      throw new Error("Incomplete data for create address!");
    }

    const address = this.addressesRepository.create({
      user_id,
      street,
      number,
      district,
      city,
      state,
      zipcode,
      lat,
      long,
      active: 1,
    });

    await this.addressesRepository.save(address);

    return address;
  }
}

export default ADDRESS_CreateService;
