import { Repository, getCustomRepository } from "typeorm";

import { SubcategoriesUser } from "../entities/SubcategoriesUser";
import { Subcategory } from "../entities/Subcategory";
import { User } from "../entities/User";
import { SubcategoriesUserRepository } from "../repositories/SubcategoriesUserRepository";
import { SubcategoryRepository } from "../repositories/SubcategoryRepository";
import { UserRepository } from "../repositories/UserRepository";

interface IRequest {
  user_id: number;
  subcategory_id: number;
  price: number;
}

class USER_CreateSubcategoryService {
  private usersRepository: Repository<User>;
  private subcategoryRepository: Repository<Subcategory>;
  private subcategoryUserRepository: Repository<SubcategoriesUser>;

  constructor() {
    this.usersRepository = getCustomRepository(UserRepository);
    this.subcategoryRepository = getCustomRepository(SubcategoryRepository);
    this.subcategoryUserRepository = getCustomRepository(
      SubcategoriesUserRepository
    );
  }

  public async execute({
    user_id,
    subcategory_id,
    price,
  }: IRequest): Promise<SubcategoriesUser> {
    if (!user_id || !subcategory_id || !price) {
      throw new Error("Incomplete data");
    }

    const user = await this.usersRepository.findOne({
      where: { id: user_id },
    });

    if (!user) {
      throw new Error("User is not exists");
    }

    const subcategory = await this.subcategoryRepository.findOne({
      where: { id: subcategory_id },
    });

    if (!subcategory) {
      throw new Error("Subcategory is not exists");
    }

    const sub = this.subcategoryUserRepository.create({
      user_id,
      subcategory_id,
      price,
    });

    await this.subcategoryUserRepository.save(sub);

    return sub;
  }
}

export default USER_CreateSubcategoryService;
