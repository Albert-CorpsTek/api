import { hash } from "bcryptjs";
import { Repository, getCustomRepository } from "typeorm";

import { User } from "../entities/User";
import { UserRepository } from "../repositories/UserRepository";

interface IRquest {
  plan_id: number;
  user_type_id: number;
  name: string;
  fullname: string;
  password: string;
  email: string;
  document: string;
  phone: string;
  birth: Date;
  origin: "web" | "mobile";
}

class USER_CreateService {
  private usersRepository: Repository<User>;

  constructor() {
    this.usersRepository = getCustomRepository(UserRepository);
  }

  public async execute({
    plan_id,
    user_type_id,
    name,
    fullname,
    password,
    email,
    document,
    phone,
    birth,
    origin,
  }: IRquest): Promise<User> {
    if (
      !plan_id ||
      !user_type_id ||
      !name ||
      !fullname ||
      !password ||
      !email ||
      !document ||
      !phone ||
      !birth ||
      !origin
    ) {
      throw new Error("Incomplete data for create user!");
    }

    const userExists = await this.usersRepository.findOne({
      where: [{ email }, { phone }],
    });

    if (userExists) {
      throw new Error("Phone or Mail already exists!");
    }

    const active = user_type_id !== 3 ? 1 : 0;

    const hashedPassword = await hash(password, 8);

    const user = this.usersRepository.create({
      plan_id,
      user_type_id,
      name,
      fullname,
      password: hashedPassword,
      email,
      document,
      phone,
      birth,
      origin,
      active,
      lead: 0,
      isAdmin: user_type_id === 1 ? 1 : 0,
    });

    await this.usersRepository.save(user);

    return user;
  }
}

export default USER_CreateService;
