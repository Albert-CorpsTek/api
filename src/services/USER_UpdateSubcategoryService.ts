import { Repository, getCustomRepository } from "typeorm";

import { SubcategoriesUser } from "../entities/SubcategoriesUser";
import { Subcategory } from "../entities/Subcategory";
import { User } from "../entities/User";
import { SubcategoriesUserRepository } from "../repositories/SubcategoriesUserRepository";
import { SubcategoryRepository } from "../repositories/SubcategoryRepository";
import { UserRepository } from "../repositories/UserRepository";

interface IRequest {
  user_id: number;
  subcategoryUser_id: number;
  price: number;
}

class USER_UpdateSubcategoryService {
  private usersRepository: Repository<User>;
  private subcategoryUserRepository: Repository<SubcategoriesUser>;

  constructor() {
    this.usersRepository = getCustomRepository(UserRepository);
    this.subcategoryUserRepository = getCustomRepository(
      SubcategoriesUserRepository
    );
  }

  public async execute({
    user_id,
    subcategoryUser_id,
    price,
  }: IRequest): Promise<SubcategoriesUser> {
    if (!user_id || !subcategoryUser_id || !price) {
      throw new Error("Incomplete data");
    }

    const user = await this.usersRepository.findOne({
      where: { id: user_id },
    });

    if (!user) {
      throw new Error("User is not exists");
    }

    const subcategoryUser = await this.subcategoryUserRepository.findOne({
      where: { id: subcategoryUser_id },
    });

    if (!subcategoryUser) {
      throw new Error("Subcategory is not exists for user");
    }

    subcategoryUser.price = price;

    await this.subcategoryUserRepository.save(subcategoryUser);

    return subcategoryUser;
  }
}

export default USER_UpdateSubcategoryService;
