import { Repository, getCustomRepository } from "typeorm";

import { Schedule } from "../entities/Schedule";
import { User } from "../entities/User";
import { ScheduleRepository } from "../repositories/ScheduleRepository";
import { UserRepository } from "../repositories/UserRepository";

interface IRquest {
  user_id: number;
  weekday_id: number;
  start_time?: string;
  finish_time?: string;
}

class USER_UpdateScheduleService {
  private scheduleRepository: Repository<Schedule>;
  private userRepository: Repository<User>;

  constructor() {
    this.scheduleRepository = getCustomRepository(ScheduleRepository);
    this.userRepository = getCustomRepository(UserRepository);
  }

  public async execute({
    user_id,
    weekday_id,
    start_time,
    finish_time,
  }: IRquest): Promise<Schedule> {
    const userExists = await this.userRepository.findOne({
      where: { id: user_id },
    });

    if (!userExists) {
      throw new Error("User does not exist");
    }
    const weekday = await this.scheduleRepository.findOne({
      where: { id: weekday_id },
    });

    if (!start_time && !finish_time) {
      weekday.active = 0;
      await this.scheduleRepository.save(weekday);
    } else {
      weekday.start_time = start_time;
      weekday.finish_time = finish_time;
      await this.scheduleRepository.save(weekday);
    }
    return weekday;
  }
}

export default USER_UpdateScheduleService;
