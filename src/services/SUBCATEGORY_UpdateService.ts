import { Repository, getCustomRepository } from "typeorm";

import { Subcategory } from "../entities/Subcategory";
import { User } from "../entities/User";
import { SubcategoryRepository } from "../repositories/SubcategoryRepository";
import { UserRepository } from "../repositories/UserRepository";

interface IRequest {
  user_id: number;
  name?: string;
  subcategory_id: number;
  description?: string;
}

class SUBCATEGORY_UpdateService {
  private usersRepository: Repository<User>;
  private SubcategoryRepository: Repository<Subcategory>;

  constructor() {
    this.usersRepository = getCustomRepository(UserRepository);
    this.SubcategoryRepository = getCustomRepository(SubcategoryRepository);
  }

  public async execute({
    user_id,
    subcategory_id,
    name,
    description,
  }: IRequest): Promise<Subcategory> {
    if (!user_id || !subcategory_id) {
      throw new Error("Incomplete data");
    }

    const user = await this.usersRepository.findOne({
      where: { id: user_id, isAdmin: 1 },
    });

    if (!user) {
      throw new Error("User is not exists/is not admin");
    }

    const subcategory = await this.SubcategoryRepository.findOne({
      where: { id: subcategory_id },
    });

    subcategory.name = name;
    subcategory.description = description;

    await this.SubcategoryRepository.save(subcategory);

    return subcategory;
  }
}

export default SUBCATEGORY_UpdateService;
