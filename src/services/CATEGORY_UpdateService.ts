import { Repository, getCustomRepository } from "typeorm";

import { Category } from "../entities/Category";
import { User } from "../entities/User";
import { CategoryRepository } from "../repositories/CategoryRepository";
import { UserRepository } from "../repositories/UserRepository";

interface IRequest {
  user_id: number;
  name: string;
  category_id: number;
  icon_url: string;
}

class CATEGORY_UpdateService {
  private usersRepository: Repository<User>;
  private categoryRepository: Repository<Category>;

  constructor() {
    this.usersRepository = getCustomRepository(UserRepository);
    this.categoryRepository = getCustomRepository(CategoryRepository);
  }

  public async execute({
    user_id,
    category_id,
    name,
    icon_url,
  }: IRequest): Promise<Category> {
    if (!user_id || !name || !category_id || !icon_url) {
      throw new Error("Incomplete data");
    }

    const user = await this.usersRepository.findOne({
      where: { id: user_id, isAdmin: 1 },
    });

    if (!user) {
      throw new Error("User is not exists/is not admin");
    }

    const category = await this.categoryRepository.findOne({
      where: { id: category_id },
    });

    category.name = name;
    category.icon_url = icon_url;

    await this.categoryRepository.save(category);

    return category;
  }
}

export default CATEGORY_UpdateService;
