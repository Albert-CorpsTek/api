import { Repository, getCustomRepository } from "typeorm";

import { Schedule } from "../entities/Schedule";
import { User } from "../entities/User";
import { ScheduleRepository } from "../repositories/ScheduleRepository";
import { UserRepository } from "../repositories/UserRepository";

interface IRquest {
  user_id: number;
  weekday: number[];
  start_time: string;
  finish_time: string;
}

class USER_CreateScheduleService {
  private usersRepository: Repository<User>;
  private scheduleRepository: Repository<Schedule>;

  constructor() {
    this.usersRepository = getCustomRepository(UserRepository);
    this.scheduleRepository = getCustomRepository(ScheduleRepository);
  }

  public async execute({
    user_id,
    weekday,
    start_time,
    finish_time,
  }: IRquest): Promise<Schedule[]> {
    if (!user_id || weekday.length === 0 || !start_time || !finish_time) {
      throw new Error("Incomplete data for create schedule!");
    }

    const userExists = await this.usersRepository.findOne({
      where: {
        id: user_id,
      },
    });

    if (!userExists) {
      throw new Error("User not exists!");
    }

    const active = 1;

    const weekdays = Promise.all(
      weekday.map(async (d) => {
        const schedule = this.scheduleRepository.create({
          active,
          user_id,
          weekday: d,
          start_time,
          finish_time,
        });

        const sched = await this.scheduleRepository.save(schedule as Schedule);
        return sched;
      })
    );
    return weekdays;
  }
}

export default USER_CreateScheduleService;
