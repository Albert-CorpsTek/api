import { Repository, getCustomRepository } from "typeorm";

import { Category } from "../entities/Category";
import { User } from "../entities/User";
import { CategoryRepository } from "../repositories/CategoryRepository";
import { UserRepository } from "../repositories/UserRepository";

interface IRequest {
  user_id: number;
}

class CATEGORY_ListService {
  private usersRepository: Repository<User>;
  private categoryRepository: Repository<Category>;

  constructor() {
    this.usersRepository = getCustomRepository(UserRepository);
    this.categoryRepository = getCustomRepository(CategoryRepository);
  }

  public async execute({ user_id }: IRequest): Promise<Category[]> {
    if (!user_id) {
      throw new Error("Incomplete data");
    }

    const user = await this.usersRepository.findOne({
      where: { id: user_id },
    });

    if (!user) {
      throw new Error("User is not exists/is not admin");
    }

    const categories = this.categoryRepository.find();

    return categories;
  }
}

export default CATEGORY_ListService;
