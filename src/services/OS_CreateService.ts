import { Repository, getCustomRepository } from "typeorm";

import { OrderService } from "../entities/OrderService";
import { User } from "../entities/User";
import { OrderServiceRepository } from "../repositories/OrderServiceRepository";
import { UserRepository } from "../repositories/UserRepository";

interface IOSProps {
  subcat_tec_id: number;
  tec_id: number;
  value: number;
  note?: string;
  status?:
    | "requisitada"
    | "aceita"
    | "negada"
    | "em andamento"
    | "finalizada"
    | "avaliada";
}

interface IRequest {
  user_id: number;
  data: IOSProps[];
}

class SUBCATEGORY_CreateService {
  private usersRepository: Repository<User>;
  private orderServiceRepository: Repository<OrderService>;

  constructor() {
    this.usersRepository = getCustomRepository(UserRepository);
    this.orderServiceRepository = getCustomRepository(OrderServiceRepository);
  }

  public async execute({
    user_id,
    data,
  }: IRequest): Promise<OrderService | OrderService[]> {
    if (data.length === 0 || !user_id) {
      throw new Error("Incomplete data");
    }

    const user = await this.usersRepository.findOne({
      where: { id: user_id },
      relations: ["address"],
    });

    if (!user) {
      throw new Error("User is not exists");
    }

    if (data.length === 1) {
      const tec = await this.usersRepository.findOne({
        where: { id: data[0].tec_id, user_type_id: 3 },
      });

      if (!tec) {
        throw new Error("User is not exists/is not tec");
      }

      const os = this.orderServiceRepository.create({
        user_id,
        subcat_tec_id: data[0].subcat_tec_id,
        tec_id: data[0].tec_id,
        value: data[0].value,
        address_id: user.address.id,
        note: data[0].note,
        status: "requisitada",
      });

      await this.orderServiceRepository.save(os);
      return os;
    }

    const listOs = Promise.all(
      data.map(async (u) => {
        const os = this.orderServiceRepository.create({
          user_id,
          subcat_tec_id: u.subcat_tec_id,
          tec_id: u.tec_id,
          value: u.value,
          address_id: user.address.id,
          note: u.note,
          status: "requisitada",
        });
        await this.orderServiceRepository.save(os);
        return os;
      })
    );

    return listOs;
  }
}

export default SUBCATEGORY_CreateService;
