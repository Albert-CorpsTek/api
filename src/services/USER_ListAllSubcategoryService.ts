import { Repository, getCustomRepository } from "typeorm";

import { SubcategoriesUser } from "../entities/SubcategoriesUser";
import { User } from "../entities/User";
import { SubcategoriesUserRepository } from "../repositories/SubcategoriesUserRepository";
import { UserRepository } from "../repositories/UserRepository";

interface IRequest {
  user_id: number;
}

class USER_ListAllSubcategoryService {
  private usersRepository: Repository<User>;
  private subcategoryUserRepository: Repository<SubcategoriesUser>;

  constructor() {
    this.usersRepository = getCustomRepository(UserRepository);
    this.subcategoryUserRepository = getCustomRepository(
      SubcategoriesUserRepository
    );
  }

  public async execute({ user_id }: IRequest): Promise<SubcategoriesUser[]> {
    if (!user_id) {
      throw new Error("Incomplete data");
    }

    const user = await this.usersRepository.findOne({
      where: { id: user_id },
    });

    if (!user) {
      throw new Error("User is not exists");
    }

    const subcategories = await this.subcategoryUserRepository.find({
      relations: ["subcategory", "subcategory.category", "user"],
    });

    return subcategories;
  }
}

export default USER_ListAllSubcategoryService;
